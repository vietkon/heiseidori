const gulp = require("gulp");
const sync = require("browser-sync");
const sass = require("gulp-sass");
const pleeease = require("gulp-pleeease");
const pug = require("gulp-pug");
const babel = require("gulp-babel");
const plumber = require("gulp-plumber");
const rename = require("gulp-rename");
const sourcemaps = require("gulp-sourcemaps");
const uglify = require("gulp-uglify");
const notifier = require("node-notifier");
const sequence = require("run-sequence");
const es2015 = require('babel-preset-es2015');

// 改行コード変更
const crLfReplace = require('gulp-cr-lf-replace');
// 文字コード変更
// const replace = require('gulp-replace');
const convertEncoding = require('gulp-convert-encoding');
/**
 * Scss
 */
// gulp.task("scss", function () {
//     return gulp.src("src/**/*.scss")
//         .pipe(sass())
//         .pipe(pleeease({
//             autoprefixer: {
//                 browsers: ["last 2 versions", "ie 10", "android 4.4"]
//             },
//             // minifier: false,
//             rem: false,
//             minifier: true,
//             mqpacker: true
//         }))
//         .pipe(sourcemaps.write())
//         .pipe(rename({extname: '.min.css'}))
//         .pipe(gulp.dest("htdocs"));
//
// });

gulp.task('scss', function () {
    return gulp.src('src/**/*.scss')
        .pipe(sourcemaps.init())
        .pipe(sass().on('error', sass.logError))
        .pipe(sourcemaps.write())
        .pipe(gulp.dest("htdocs"));
});

/**
 * pug
 */
// gulp.task("pug", function () {
//     return gulp.src(["src/pug/**/*.pug", "!src/pug/**/_*.pug"])
//         .pipe(pug({
//             basedir: "src/pug/",
//             pretty: true
//         }))
//         .pipe(crLfReplace({
//             changeCode: "CR+LF"
//         }))
//         //.pipe(replace('utf-8', 'shift_jis'))
//         .pipe(convertEncoding({to: 'shift_jis'}))
//         .pipe(gulp.dest("htdocs"));
// });

gulp.task("pug", function () {
    return gulp.src(["src/pug/**/*.pug", "!src/pug/**/_*.pug"])
        .pipe(pug({
            basedir: "src/pug/",
            pretty: true
        }))
        .on('error', function (err) {
            process.stderr.write(err.message + '\n');
            this.emit('end');
        })
        .pipe(crLfReplace({
            changeCode: "CR+LF"
        }))
        //.pipe(replace('utf-8', 'shift_jis'))
        .pipe(convertEncoding({to: 'shift_jis'}))
        .pipe(gulp.dest("htdocs"));
});


/**
 * JS ES2015
 */
gulp.task("babel", function () {
    return gulp.src(["src/**/*.babel.js"])
        .pipe(plumber({
            errorHandler: function (error) {
                console.error(error.message);
                notifier.notify(error.stack);
            }
        }))
        .pipe(sourcemaps.init())
        .pipe(babel({
            presets: ["es2015"]
        }))
        .pipe(rename(function (path) {
            path.basename = path.basename.slice(0, path.basename.lastIndexOf(".babel"))
        }))
        // 未圧縮版出力(.js)
        // .pipe(gulp.dest("htdocs"))
        .pipe(uglify())
        .pipe(rename({
            extname: '.min.js'
        }))
        // .pipe(sourcemaps.write())
        // 圧縮版出力(.min.js)
        .pipe(gulp.dest("htdocs"));
});

/**
 * browser-sync
 */
gulp.task("browser-sync", function () {
    sync({
        server: {
            baseDir: "./htdocs"
        }
    });
});

/**
 * reload
 */
gulp.task('reload', function () {
    sync.reload();
});


// ---------------------

gulp.task("watch", function () {
    gulp.watch("src/**/*.scss", ["scss"]);
    gulp.watch("src/**/*.pug", ["pug"]);
    gulp.watch("htdocs/**/*.html", ["reload"]);
    gulp.watch("htdocs/**/*.css", ["reload"]);
    gulp.watch(["src/**/*.babel.js"], function () {
        sequence(["babel"], ["reload"]);
    });
});


gulp.task("default", ["watch", "browser-sync"]);

// ---------------------


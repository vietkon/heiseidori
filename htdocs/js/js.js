function initMap() {
    var uluru = {lat: 35.66937, lng: 139.756942};
    var map = new google.maps.Map(document.getElementById('map'), {
        zoom: 17,
        center: uluru
    });
    var marker = new google.maps.Marker({
        position: uluru,
        map: map
    });
}


$(document).ready(function(){
    $("#scrolldown a").on('click', function(event) {
        if (this.hash !== "") {
            event.preventDefault();
            var hash = this.hash;
            $('html, body').animate({
                scrollTop: $(hash).offset().top - 20
            }, 800, function(){
                window.location.hash = hash;
            });
        }
    });

    $("#scrolltop").on('click',function () {
       $('html,body').animate({
           scrollTop:0
       }) ;
    });
});

new WOW().init();